import firebase from "firebase/app";
import "firebase/auth";
import "firebase/firestore";
// Check authentication status
var auth = firebase.auth();
var firestore = firebase.firestore();
var storageRef, user;
var wgLogout = document.getElementById('wg-logout');
var loginForm = document.querySelector('form[name="loginForm"]');
var registerForm = document.querySelector('form[name="registerForm"]');
var resetPasswordForm = document.querySelector('form[name="resetPasswordForm"]');
var updateUserForm = document.querySelector('form[name="updateUserForm"]');

auth.onAuthStateChanged((_user) => {
    user = _user;
    console.log('+++++', user);
    if(user) {
        localStorage.setItem('uid', user.uid);
        // setUserInfo();
        if(location.href.indexOf('login') > -1 && location.href.indexOf('register') > -1) {    
            location.href = 'index.html';
        }
    } else {
        // if(!(location.href.indexOf('login') !== -1 && location.href.indexOf('register') !== -1)) {    
        //     location.href = 'login.html';
        // }
    }
});

if(wgLogout) {
    wgLogout.addEventListener('click', function logout() {
        auth
            .signOut()
            .then(() => {
                user = null;
            })
            .catch((error) => errorHandler(error));;
    });
}

if(loginForm) {
    loginForm.addEventListener('submit', function(e) {
        e.preventDefault();
        loading(true);
        var form = e.target;
        var email = form.email.value;
        var password = form.password.value;
    
        auth
            .signInWithEmailAndPassword(email, password)
            .then((res) => {
                localStorage.setItem('uid', res.user.uid);
                loading();
            })
            .catch((error) => errorHandler(error));
    });
}

if(registerForm) {
    registerForm.addEventListener('submit', function(e) {
        e.preventDefault();
        var form = e.target;
        var email = form.email.value;
        var password = form.password.value;
        loading(true);
    
        auth
            .createUserWithEmailAndPassword(email, password)
            .then(() => {loading();})
            .catch((error) => errorHandler(error));
    });
}

if(resetPasswordForm) {
    resetPasswordForm.addEventListener('submit', function(e) {
        e.preventDefault();
        var form = e.target;
        var email = form.email.value;
        
        auth
            .sendPasswordResetEmail(email)
            .then(() => {})
            .catch((error) => errorHandler(error));
    });
}


if(updateUserForm) {
    updateUserForm.addEventListener('submit', function(e) {
        e.preventDefault();
        var form = e.target;
        var username = form.username.value;
        var description = form.description.value;
        
        firestore
            .collection('core/data/users')
            .doc(user.uid)
            .set({
                username,
                description
            })
            .then(() => {})
            .catch((error) => errorHandler(error));
    });
}

// function setUserInfo() {
//     firestore.collection('core/data/users').doc(user.uid).get().then(_ => {
//         var data = _.data() || {};
//         document.forms['userForm']['username'].value = data.username;
//         document.forms['userForm']['description'].value = data.description;
//     });
// }

function uploadUserImg(img) {
    storageRef.put(img.target.files[0]).then((snapshot) => {
        console.log('Uploaded a blob or file!', snapshot);
    });
}
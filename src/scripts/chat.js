import firebase from "firebase/app";
import "firebase/auth";
import "firebase/firestore";
// Check authentication status
var firestore = firebase.firestore();
var storageRef;
var messageForm = document.querySelector('form[name="messageForm"]');
var uid = localStorage.getItem('uid');
var wgMessages = document.getElementById('wg-messages');
firestore
    .collection('core/data/users/' + uid + '/messages/user_1/chat')
    .onSnapshot(function(snapshot) {
        if(snapshot && uid) {
            snapshot.docChanges().forEach(function(change) {
                if(change && change.doc.data()) {
                    console.log(change.doc.data());
                    // <li class="d-flex mb-3">
                    //     <div>
                    //       <img class="rounded-circle" width="50" height="50" src="https://uifaces.co/our-content/donated/xZ4wg2Xj.jpg" alt="">
                    //     </div>
                    //     <div class="mx-2">
                    //       <p class="bg-light p-2 rounded m-0">Hi Henry!!!</p>
                    //       <div>9h ago</div>
                    //     </div>
                    //   </li>
                    var li = document.createElement('li');
                    li.classList = 'd-flex mb-3';
                    li.innerHTML = `<div>
                        <img class="rounded-circle" width="50" height="50" src="https://uifaces.co/our-content/donated/xZ4wg2Xj.jpg" alt="">
                    </div>
                    <div class="mx-2">
                        <p class="bg-light p-2 rounded m-0">${change.doc.data().message}</p>
                        <div>9h ago</div>
                    </div>`;
                    
                    if(wgMessages) {
                        wgMessages.insertAdjacentElement('afterend', li);
                    }
                    // if (change.type === "added") {
                    //     console.log("New city: ", change.doc.data());
                    // }
                    // if (change.type === "modified") {
                    //     console.log("Modified city: ", change.doc.data());
                    // }
                    // if (change.type === "removed") {
                    //     console.log("Removed city: ", change.doc.data());
                    // }
                }
            });
        }
    });

if(messageForm) {
    messageForm.addEventListener('submit', function(e) {
        e.preventDefault();
        loading(true);
        var form = e.target;
        var message = form.message.value;

        firestore
            .collection('core/data/users/' + uid + '/messages/user_1/chat')
            .add({
                message,
                date: moment().format()
            })
            .then(() => {loading();})
            .catch((error) => errorHandler(error));
    });
};

function uploadUserImg(img) {
    storageRef.put(img.target.files[0]).then((snapshot) => {
        console.log('Uploaded a blob or file!', snapshot);
    });
};

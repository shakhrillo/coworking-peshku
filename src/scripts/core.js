import firebase from "firebase/app";
import "firebase/auth";
import "firebase/firestore";

var firebaseConfig = {
    apiKey: "AIzaSyBxdjHBxbeC8BXWv8rja2Msjkn-raAJb5Q",
    authDomain: "migrant365-cabba.firebaseapp.com",
    databaseURL: "https://migrant365-cabba.firebaseio.com",
    projectId: "migrant365-cabba",
    storageBucket: "migrant365-cabba.appspot.com",
    messagingSenderId: "113102157253",
    appId: "1:113102157253:web:7649e43eb237d1d3efc7ac",
    measurementId: "G-H57MW0Z2QX"
};

firebase.initializeApp(firebaseConfig);

window.errorHandler = function(error) {
    console.error(error);
    loading();
};

window.loading = function(value) {
    var buttons = document.querySelectorAll('button');
    var inputs = document.querySelectorAll('input');
    if(inputs.length) {
        if(value) {
            inputs.forEach(function(button) {
                button.setAttribute('disabled', '');
            });
        } else {
            inputs.forEach(function(button) {
                button.removeAttribute('disabled');
            });
        };
    };

    if(buttons.length) {
        if(value) {
            buttons.forEach(function(button) {
                button.setAttribute('disabled', '');
            });
        } else {
            buttons.forEach(function(button) {
                button.removeAttribute('disabled');
            });
        };
    };
};
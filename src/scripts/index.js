import '../styles/index.scss';
if (process.env.NODE_ENV === 'development') {
  require('../index.html');
}
moment.tz.setDefault("America/New_York");
import '../scripts/core';
import '../scripts/auth';
import '../scripts/chat';

const Path = require('path');
const Webpack = require('webpack');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  entry: {
    app: Path.resolve(__dirname, '../src/scripts/index.js'),
  },
  output: {
    path: Path.join(__dirname, '../build'),
    filename: 'js/[name].js',
  },
  optimization: {
    splitChunks: {
      chunks: 'all',
      name: false,
    },
  },
  plugins: [
    new CleanWebpackPlugin(),
    new Webpack.ProvidePlugin({
      moment: 'moment-timezone',
    }),
    new CopyWebpackPlugin({ patterns: [{ from: Path.resolve(__dirname, '../public'), to: 'public' }] }),
    new HtmlWebpackPlugin({
      template: Path.resolve(__dirname, '../src/index.html'),
    }),
    new HtmlWebpackPlugin({
      filename: 'chat.html',
      template: Path.resolve(__dirname, '../src/chat.html'),
    }),
    new HtmlWebpackPlugin({
      filename: 'chat-1.html',
      template: Path.resolve(__dirname, '../src/chat-1.html'),
    }),
    new HtmlWebpackPlugin({
      filename: 'chat-2.html',
      template: Path.resolve(__dirname, '../src/chat-2.html'),
    }),
    new HtmlWebpackPlugin({
      filename: 'chat-3.html',
      template: Path.resolve(__dirname, '../src/chat-3.html'),
    }),

    new HtmlWebpackPlugin({
      filename: 'chat-4.html',
      template: Path.resolve(__dirname, '../src/chat-4.html'),
    }),
    new HtmlWebpackPlugin({
      filename: 'profile.html',
      template: Path.resolve(__dirname, '../src/profile.html'),
    }),
    new HtmlWebpackPlugin({
      filename: 'settings.html',
      template: Path.resolve(__dirname, '../src/settings.html'),
    }),
    new HtmlWebpackPlugin({
      filename: 'login.html',
      template: Path.resolve(__dirname, '../src/login.html'),
    }),
    new HtmlWebpackPlugin({
      filename: 'register.html',
      template: Path.resolve(__dirname, '../src/register.html'),
    }),
    new HtmlWebpackPlugin({
      filename: 'reset-password.html',
      template: Path.resolve(__dirname, '../src/reset-password.html'),
    }),
    new HtmlWebpackPlugin({
      filename: 'landing.html',
      template: Path.resolve(__dirname, '../src/landing.html'),
    }),
  ],
  resolve: {
    alias: {
      '~': Path.resolve(__dirname, '../src'),
    },
  },
  module: {
    rules: [
      {
        test: /\.mjs$/,
        include: /node_modules/,
        type: 'javascript/auto',
      },
      {
        test: /\.(ico|jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2)(\?.*)?$/,
        use: {
          loader: 'file-loader',
          options: {
            name: '[path][name].[ext]',
          },
        },
      },
    ],
  },
};
